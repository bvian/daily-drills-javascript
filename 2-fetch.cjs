
/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/

const userApi = "https://jsonplaceholder.typicode.com/users";
const todoApi = "https://jsonplaceholder.typicode.com/todos";

// Fetching data from Api

function fetchApi(apiToFetch) {

    return fetch(apiToFetch)
        .then((response) => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error(`${response.status}`);
            }
        })
        .catch((err) => {
            Promise.reject(err);
        });
}

// Fetching all users data by userId 

function fetchUserDetailById(id, apiToFetch) {

    return new Promise((resolve, reject) => {
        if (id === undefined) {
            const err = new Error('Id must be passed');
            reject(err);
        }
        if (typeof id !== 'number') {
            const err = new Error('Id must be of type number');
            reject(err);
        }

        fetchApi(apiToFetch)
            .then((data) => {
                resolve(data);
            })
            .catch((err) => {
                reject(err);
            });
    });
}

// Getting all data from all user ids

function allDetailsByUserId(usersData) {

    const userIds = usersData.map((user) => {
        return user.id;
    });

    const usersInfoByIds = userIds.map((userId) => {
        const userUrlById = `${userApi}?id=${userId}`;
        return fetchUserDetailById(userId, userUrlById);
    });

    return Promise.all(usersInfoByIds);
}

// Question 1

function question1() {

    fetchApi(userApi)
        .then((data) => {
            console.log(data);
            console.log("user data is fetched.");
        })
        .catch((err) => {
            console.error(err);
        });
}

// Question 2

function question2() {

    fetchApi(todoApi)
        .then((data) => {
            console.log(data);
            console.log("todo data is fetched");
        })
        .catch((err) => {
            console.error(err);
        });
}

// Question 3

function question3() {

    fetchApi(userApi)
        .then((data) => {
            console.log(data);
            console.log("user data is fetched.");
            return fetchApi(todoApi);
        })
        .then((data) => {
            console.log(data);
            console.log("todo data is fetched");
        })
        .catch((err) => {
            console.error(err);
        });
}

// Question 4

function question4() {

    fetchApi(userApi)
        .then((data) => {
            console.log(data);
            console.log("user data is fetched.");
            return allDetailsByUserId(data);
        })
        .then((data) => {
            console.log(data);
            console.log("all user data is fetched by all userIds");
        })
        .catch((err) => {
            console.error(err);
        });
}

// Queetion 5

function question5() {

    fetchApi(`${todoApi}?id=1`)
        .then((data) => {
            console.log(data);
            console.log("todo data is fetched for user id 1.");
            const userUrlById = `${userApi}?id=${data[0].userId}`;
            return fetchApi(userUrlById);
        })
        .then((data) => {
            console.log(data);
            console.log("user data is fetched by userId from todo api.");
        })
        .catch((err) => {
            console.error(err);
        });
}

// Question 1. Fetch all the users

question1();

// Question 2. Fetch all the todos

question2();

// Question 3. Use the promise chain and fetch the users first and then the todos.

question3();

// Question 4. Use the promise chain and fetch the users first and then all the details for each user.

question4();

// Question 5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo.

question5();