const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/

// Question 1

function getItemsAvailable() {

    const allItemsWithAvailable = items.filter((item) => {
        return item.available === true;
    });

    return allItemsWithAvailable;
}

// Question 2

function getItemsVitaminC() {

    const allItemsWithVitaminC = items.filter((item) => {
        return item.contains === "Vitamin C";
    });

    return allItemsWithVitaminC;
}

// Question 3

function getItemsVitaminA() {

    const allItemsWithVitaminA = items.filter((item) => {
        return item.contains.includes("Vitamin A");
    });

    return allItemsWithVitaminA;
}

// Question 4.

function groupItemsInVitamin() {

    const allItemsInVitamin = items.reduce((groupItemsVitamin, item) => {

        let vitamins = item.contains.split(", ");

        vitamins = vitamins.map((vitamin) => {
            if (groupItemsVitamin[vitamin] === undefined) {
                groupItemsVitamin[vitamin] = [item.name];

            } else {
                groupItemsVitamin[vitamin].push(item.name);
            }

            return vitamin;
        });

        return groupItemsVitamin;

    }, {});

    return allItemsInVitamin;
}

// Question 5

function sortInVitamins() {

    const allSortedItems = items.sort((item1, item2) => {

        const vitamins1 = item1.contains.split(", ");
        const vitamins2 = item2.contains.split(", ");

        return vitamins2.length - vitamins1.length;
    });

    return allSortedItems;
}

// Question 1. Get all items that are available 

const allItemsAvailable = getItemsAvailable();
console.log(allItemsAvailable);

// Question 2. Get all items containing only Vitamin C.

const allItemsVitaminC = getItemsVitaminC();
console.log(allItemsVitaminC);

// Question 3. Get all items containing Vitamin A.

const allItemsVitaminA = getItemsVitaminA();
console.log(allItemsVitaminA);

// Question 4. Group items based on the Vitamins that they contain in the following format:

const allItemsInVitamin = groupItemsInVitamin();
console.log(allItemsInVitamin);

// Question 5. Sort items based on number of Vitamins they contain.

const sortAllInVitamins = sortInVitamins();
console.log(sortAllInVitamins);