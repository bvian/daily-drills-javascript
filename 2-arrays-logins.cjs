const allLoginData = [{
    "id": 1,
    "first_name": "Valera",
    "last_name": "Pinsent",
    "email": "vpinsent0@google.co.jp",
    "gender": "Male",
    "ip_address": "253.171.63.171"
},
{
    "id": 2,
    "first_name": "Kenneth",
    "last_name": "Hinemoor",
    "email": "khinemoor1@yellowbook.com",
    "gender": "Polygender",
    "ip_address": "50.231.58.150"
},
{
    "id": 3,
    "first_name": "Roman",
    "last_name": "Sedcole",
    "email": "rsedcole2@addtoany.com",
    "gender": "Genderqueer",
    "ip_address": "236.52.184.83"
},
{
    "id": 4,
    "first_name": "Lind",
    "last_name": "Ladyman",
    "email": "lladyman3@wordpress.org",
    "gender": "Male",
    "ip_address": "118.12.213.144"
},
{
    "id": 5,
    "first_name": "Jocelyne",
    "last_name": "Casse",
    "email": "jcasse4@ehow.com",
    "gender": "Agender",
    "ip_address": "176.202.254.113"
},
{
    "id": 6,
    "first_name": "Stafford",
    "last_name": "Dandy",
    "email": "sdandy5@exblog.jp",
    "gender": "Female",
    "ip_address": "111.139.161.143"
},
{
    "id": 7,
    "first_name": "Jeramey",
    "last_name": "Sweetsur",
    "email": "jsweetsur6@youtube.com",
    "gender": "Genderqueer",
    "ip_address": "196.247.246.106"
},
{
    "id": 8,
    "first_name": "Anna-diane",
    "last_name": "Wingar",
    "email": "awingar7@auda.org.au",
    "gender": "Agender",
    "ip_address": "148.229.65.98"
},
{
    "id": 9,
    "first_name": "Cherianne",
    "last_name": "Rantoul",
    "email": "crantoul8@craigslist.org",
    "gender": "Genderfluid",
    "ip_address": "141.40.134.234"
},
{
    "id": 10,
    "first_name": "Nico",
    "last_name": "Dunstall",
    "email": "ndunstall9@technorati.com",
    "gender": "Female",
    "ip_address": "37.12.213.144"
}
]

/* 

1. Find all people who are Agender
2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
3. Find the sum of all the second components of the ip addresses.
3. Find the sum of all the fourth components of the ip addresses.
4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
5. Filter out all the .org emails
6. Calculate how many .org, .au, .com emails are there
7. Sort the data in descending order of first name

NOTE: Do not change the name of this file

*/

// Question 1
function getAgender() {

    const allAgenderData = allLoginData.filter((personLoginData) => {
        return personLoginData.gender === "Agender";
    });

    return allAgenderData;
}

// Question 2
function splitIPAddress() {

    allLoginData.forEach((personLoginData) => {

        personLoginData.ip_address = personLoginData.ip_address.split(".")
            .map((splitIPAddress) => {
                return Number(splitIPAddress);
            });
    });
}

// Question 3
function sumSecondComponentIPAddress() {

    const sumOfAllSecondIP = allLoginData.reduce((sumOfSecondIP, personLoginData) => {

        sumOfSecondIP += personLoginData.ip_address[1];
        return sumOfSecondIP;

    }, 0);

    return sumOfAllSecondIP;
}

// Question 3
function sumFourthComponentIPAddress() {

    const sumOfAllFourthIP = allLoginData.reduce((sumOfFourthIP, personLoginData) => {

        sumOfFourthIP += personLoginData.ip_address[3];
        return sumOfFourthIP;

    }, 0);

    return sumOfAllFourthIP;
}

// Question 4
function addNewKeyFullName() {

    allLoginData.forEach((personLoginData) => {
        personLoginData.full_name = `${personLoginData.first_name} ${personLoginData.last_name}`;
    });
}

//Question 5
function filterAllOrgEmails() {

    const filterOutOrgEmails = allLoginData.filter((personLoginData) => {
        return personLoginData.email.endsWith(".org");
    });

    return filterOutOrgEmails;
}

//Question 6
function getSpecificEmailsCount() {

    const specificEmailsCount = allLoginData.reduce((countEmails, personLoginData) => {

        const emailExtensions = [".org", ".au", ".com"];

        emailExtensions.forEach((extension) => {
            if (personLoginData.email.endsWith(extension)) {
                countEmails += 1;
            }
        });

        return countEmails;
    }, 0);

    return specificEmailsCount;
}

// Question 7
function sortFirstNameInDescOrder() {

    const sortFirstNameInDesc = allLoginData.sort((personLoginData1, personLoginData2) => {

        if (personLoginData1.first_name < personLoginData2.first_name) {
            return 1;
        } else {
            return -1;
        }
    });

    return sortFirstNameInDesc;
}

// Question 1. Find all people who are Agender
const allAgenderData = getAgender();
console.log(allAgenderData);

// Question 2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
splitIPAddress();
console.log(allLoginData);

// Question 3. Find the sum of all the second components of the ip addresses.
const allSumOfSecondIP = sumSecondComponentIPAddress();
console.log(allSumOfSecondIP);

// Question 3. Find the sum of all the fourth components of the ip addresses.
const allSumOfFourthIP = sumFourthComponentIPAddress();
console.log(allSumOfFourthIP);

// Question 4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
addNewKeyFullName();
console.log(allLoginData);

// Question 5. Filter out all the .org emails
const filterOutOrgEmails = filterAllOrgEmails();
console.log(filterOutOrgEmails);

// Question 6. Calculate how many .org, .au, .com emails are there
const specificEmailsCount = getSpecificEmailsCount();
console.log(specificEmailsCount);

// Question 7. Sort the data in descending order of first name
const sortFirstNameInDesc = sortFirstNameInDescOrder();
console.log(sortFirstNameInDesc);