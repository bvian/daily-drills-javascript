const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
};


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/

// Question 1.

function getUserInterestVideoGame() {

    const allUserInterestVideoGame = Object.entries(users).
        filter((user) => {

            const userInterest = user[1].interest || user[1].interests;

            const getInterest = userInterest[0].split(',')
                .map((interest) => {
                    if (interest.includes("Video Games")) {
                        return interest;
                    }
                })
                .join(' ');

            return getInterest.includes("Video Games");
        });

    return Object.fromEntries(allUserInterestVideoGame);
}

// Question 2

function getUserInGermany() {

    const allUserInGermany = Object.entries(users)
        .filter((user) => {

            const userNationality = user[1].nationality;
            return userNationality.includes("Germany");
        });

    return Object.fromEntries(allUserInGermany);
}

// Question 3

function getSortedUser() {

    const allSortedUser = Object.entries(users)
        .sort((user1, user2) => {

            function getPriority(designation) {

                if (designation.includes('Senior')) {
                    return 3;
                } else if (designation.includes('Developer')) {
                    return 2;
                } else {
                    return 1;
                }
            }

            const user1Priority = getPriority(user1[1].desgination);
            const user2Priority = getPriority(user2[1].desgination);

            return (user2Priority - user1Priority) || (user2[1].age - user1[1].age);
        });

    return Object.fromEntries(allSortedUser);
}

// Question 4

function getUserMasterDegree() {

    const allUserMasterDegree = Object.entries(users)
        .filter((user) => {

            const userQualification = user[1].qualification;
            return userQualification.includes("Masters");
        });

    return Object.fromEntries(allUserMasterDegree);
}

// Question 5

function getUserInProgramLang() {

    const allUserInProgramLang = Object.entries(users)
        .reduce((allUserGroup, user) => {

            const userDesignation = user[1].desgination.split(' ')
                .map((designation) => {
                    if (['Golang', 'Javascript', 'Python'].includes(designation)) {
                        return designation;
                    }
                })
                .join(' ')
                .trim();

            if (allUserGroup[userDesignation] === undefined) {
                allUserGroup[userDesignation] = [];
            }
            allUserGroup[userDesignation].push(user[0]);

            return allUserGroup;
        }, {});

    return allUserInProgramLang;
}

// Q1 Find all users who are interested in playing video games.

const allUserInterestVideoGame = getUserInterestVideoGame();
console.log(allUserInterestVideoGame);

// Q2 Find all users staying in Germany.

const allUserInGermany = getUserInGermany();
console.log(allUserInGermany);

// Q3 Sort users based on their seniority level 

const allSortedUser = getSortedUser();
console.log(allSortedUser);

// Q4 Find all users with masters Degree.

const allUserMasterDegree = getUserMasterDegree();
console.log(allUserMasterDegree);

// Q5 Group users based on their Programming language mentioned in their designation.

const allUserInProgramLang = getUserInProgramLang();
console.log(allUserInProgramLang);