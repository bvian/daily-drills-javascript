const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}];


/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/

// created function for getting all items

function getAllItems(products) {

    const gettingAllItems = products.reduce((filteredItems, product) => {

        filteredItems = Object.entries(product)
            .reduce((allItems, currentItem) => {

                if (Array.isArray(currentItem[1])) {
                    allItems = filteredItems.concat(getAllItems(currentItem[1]));
                } else {

                    allItems.push({
                        [currentItem[0]]: currentItem[1]
                    });
                }
                return allItems;

            }, filteredItems);

        return filteredItems;
    }, []);

    return gettingAllItems;
}

// Question 1

function getItemsPriceMoreThan65() {

    const allItemsPriceMoreThan65 = getAllItems(products)
        .filter((product) => {
            const gettingItems = Object.entries(product)
                .filter((item) => {
                    return Number(item[1].price.replace('$', '')) > 65;
                });
            return gettingItems.length;
        });

    return allItemsPriceMoreThan65;
}

// Question 2

function getItemsQuantityMoreThan1() {

    const allItemsQuantityMoreThan1 = getAllItems(products)
        .filter((product) => {
            const gettingItems = Object.entries(product)
                .filter((item) => {
                    return item[1].quantity > 1;
                });
            return gettingItems.length;
        });

    return allItemsQuantityMoreThan1;
}

// Question 3

function getFragileItems() {

    const allFragileItems = getAllItems(products)
        .filter((product) => {
            const gettingItems = Object.entries(product)
                .filter((item) => {
                    return item[1].type !== undefined && item[1].type.includes("fragile");
                });
            return gettingItems.length;
        });

    return allFragileItems;
}

// Question 4

function getLeastAndMostExpensiveItem() {

    const sortedItems = getAllItems(products)
        .sort((product1, product2) => {
            function getPrice(product) {
                const price = Object.entries(product)
                    .map((productContents) => {
                        const priceOfSingleItem = Number(productContents[1].price.replace("$", '')) / productContents[1].quantity;
                        return priceOfSingleItem;
                    });
                return price;
            }

            const priceProduct1 = getPrice(product1);
            const priceProduct2 = getPrice(product2);

            return priceProduct1 - priceProduct2;
        });

    // return sortedItems;

    return [sortedItems[0], sortedItems[sortedItems.length - 1]];
}

// Question 5

function getGroupItemsOnMatter() {

    const groupItemsOnMatter = getAllItems(products)
        .reduce((productsInGroup, product) => {

            if (productsInGroup.Solid === undefined) {
                productsInGroup.Solid = [];
            }

            if (productsInGroup.Liquid === undefined) {
                productsInGroup.Liquid = [];
            }

            if (productsInGroup.Gas === undefined) {
                productsInGroup.Gas = [];
            }

            productsInGroup = Object.entries(product)
                .reduce((itemsInGroup, item) => {

                    if (item[0].includes("shampoo") || item[0].includes("Hair-oil")) {
                        itemsInGroup.Liquid.push(item);
                    } else {
                        itemsInGroup.Solid.push(item);
                    }

                    return itemsInGroup;

                }, productsInGroup)

            return productsInGroup;
        }, {});

    return groupItemsOnMatter;
}

// Q1. Find all the items with price more than $65.

const allItemsPriceMoreThan65 = getItemsPriceMoreThan65();
console.log(allItemsPriceMoreThan65);

// Q2. Find all the items where quantity ordered is more than 1.

const allItemsQuantityMoreThan1 = getItemsQuantityMoreThan1();
console.log(allItemsQuantityMoreThan1);

// Q.3 Get all items which are mentioned as fragile.

const allFragileItems = getFragileItems();
console.log(allFragileItems);

// Q.4 Find the least and the most expensive item for a single quantity.

const leastAndMostExpensiveItem = getLeastAndMostExpensiveItem();
console.log(leastAndMostExpensiveItem);

// Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

const groupItemsOnMatter = getGroupItemsOnMatter();
console.log(groupItemsOnMatter);