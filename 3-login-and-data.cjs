/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/


function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file

    const data = JSON.stringify(
        {
            "time stamp": new Date().toString(),
            [user.name]: activity
        },
    );

    return new Promise((resolve, reject) => {
        fs.appendFile(path.join(__dirname, "logFile.txt"), "\n" + data, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

function testLogin(passedUser) {

    login(passedUser, passedUser.id)
        .then((user) => {
            logData(user, "Login Success");
            return getData();
        })
        .then((data) => {
            console.log("data fetched", data);
            logData(passedUser, "GetData Success");
        })
        .catch((error) => {
            if (error.message.includes("User not found")) {
                logData(passedUser, "Login Failure");
            } else {
                logData(passedUser, "GetData Failure");
            }
        });
}


/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/


const fs = require('fs');
const path = require('path');

// function to read file

function readFile(filePath) {
    return new Promise((resolve, reject) => {

        fs.readFile(filePath, "utf-8", (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

// function to write file

function writeFile(filePath, data) {
    return new Promise((resolve, reject) => {

        fs.writeFile(filePath, data, ((err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        }));
    });
}

// function to delete file

function deleteFile(filePath) {
    return new Promise((resolve, reject) => {

        fs.unlink(filePath, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

// Question 1

function createAndDeleteFile() {
    const filePath1 = path.join(__dirname, 'test1.txt');
    const filePath2 = path.join(__dirname, 'test2.txt');

    const data = "My name is Bhabesh Mahato.";

    writeFile(filePath1, data)
        .then(() => {
            console.log("First file created");
        })
        .catch((err) => {
            console.error(err);
        });

    writeFile(filePath2, data)
        .then(() => {
            console.log("Second file created");
        })
        .catch((err) => {
            console.error(err);
        });

    setTimeout(() => {
        deleteFile(filePath1)
            .then(() => {
                console.log("First file deleted");
            })
            .catch((err) => {
                console.error(err);
            });

        deleteFile(filePath2)
            .then(() => {
                console.log("Second file deleted");
            })
            .catch((err) => {
                console.error(err);
            });

    }, 2 * 1000);

}

// Question 2

function readWriteAndDeleteLipsumFile() {
    const lipsumFilePath = path.join(__dirname, 'lipsum.txt');
    const newFilePath = path.join(__dirname, 'newFile.txt');

    readFile(lipsumFilePath)
        .then((data) => {
            console.log("lipsum.txt file reading successfull");
            return writeFile(newFilePath, data);
        })
        .then(() => {
            console.log("lipsum.txt file data written in newFile.txt");
            return deleteFile(lipsumFilePath);
        })
        .then(() => {
            console.log("lipsum.txt file deletion successfull");
        })
        .catch((err) => {
            console.error(err);
        });
}

/*
 Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)
*/

createAndDeleteFile();

/*
Q2. Create a new file with lipsum data (you can google and get this).
Do File Read and write data to another file
Delete the original file
Using promise chaining
*/

readWriteAndDeleteLipsumFile();

// Question 3

const user1 = {
    id: 3,
    name: "Bhabesh Mahato",
};

const user2 = {
    id: 4,
    name: "Ankit Kumar",
};

testLogin(user1);
testLogin(user2);