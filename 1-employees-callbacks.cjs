/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed.
 * If there is an error at any point, the subsequent solutions must not get executed.

 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

const fs = require("fs");

function readFile(filePath, callbackFunction) {
    fs.readFile(filePath, 'utf-8', (err, data) => {
        if (err) {
            callbackFunction(err);
        } else {
            callbackFunction(err, data);
        }
    });
}


const writeFile = (filePath, data, callbackFunction) => {
    fs.writeFile(filePath, JSON.stringify(data), (err => {
        if (err) {
            callbackFunction(err);
        } else {
            console.log('file created');
            callbackFunction();
        }
    }));
}


function getIds(data) {
    const dataOfIds = data["employees"]
        .filter((employee) => {
            return employee.id == 2 || employee.id == 13 || employee.id == 23;
        });
    return dataOfIds;
}

function getGroupData(data) {

    const groupDataBasedOnEmployees = data['employees']
        .reduce((groupData, employee) => {
            if (groupData[employee.company] === undefined) {
                groupData[employee.company] = [employee];
            } else {
                groupData[employee.company].push(employee);
            }
            return groupData;
        }, {});

    return groupDataBasedOnEmployees;
}

function getPowerpuffEmployees(data) {

    const allPowerpuffEmployees = data['employees']
        .filter((employee) => {
            return employee.company === 'Powerpuff Brigade';
        });
    return allPowerpuffEmployees;
}

function getDataExcludingId2(data) {

    const allDataExcludingId2 = data['employees']
        .filter((employee) => {
            return employee.id != 2;
        });
    return allDataExcludingId2;
}

function getSortedData(data) {

    const sortedData = data['employees']
        .sort((employee1, employee2) => {
            return (employee1.company.localeCompare(employee2.company)) || (employee1.id - employee2.id);
        });
    return sortedData;
}

function getSwapCompanies(data) {

    const companyDataId92 = data['employees'].find((employee) => {
        return employee.id === 92;
    });

    const companyDataId93 = data['employees'].find((employee) => {
        return employee.id === 93;
    });

    const swapCompany = data['employees'].map((employee) => {
        if (employee.id === 92) {
            return companyDataId93;
        } else if (employee.id === 93) {
            return companyDataId92;
        } else {
            return employee;
        }
    });

    return swapCompany;
}

function addKeyBirthdayOfIdEven(data) {
    const month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    const allEmployesAddedKeyBirthdayOfEvenId = data['employees']
        .map((employee) => {
            if (employee.id % 2 === 0) {
                const newEmployeeObject = employee;

                const date = new Date();
                newEmployeeObject.birthday = month[date.getMonth()] + " " + date.toLocaleDateString().slice(2, 4);
                return newEmployeeObject;

            } else {
                return employee;
            }
        });
    return allEmployesAddedKeyBirthdayOfEvenId;
}

// Question 1. Retrieve data for ids : [2, 13, 23].

function firstQuestion(callbackFunction) {
    readFile("./data/employeesData.json", (err, data) => {
        if (err) {
            callbackFunction(err);
        } else {
            const dataOfIds = getIds(JSON.parse(data));

            writeFile("./employees-outputs/dataOfSomeIds.json", dataOfIds, (err) => {
                if (err) {
                    callbackFunction(err);
                } else {
                    console.log("Data of some Ids written in dataOfSomeIds.json file");
                    callbackFunction();
                }
            });
        }
    });
}

// Quetion 2. Group data based on companies.

function secondQuestion(callbackFunction) {
    readFile("./data/employeesData.json", (err, data) => {
        if (err) {
            callbackFunction(err);
        } else {
            const groupDataBasedOnEmployees = getGroupData(JSON.parse(data));

            writeFile("./employees-outputs/groupDataOnCompany.json", groupDataBasedOnEmployees, (err) => {
                if (err) {
                    callbackFunction(err);
                } else {
                    console.log("Group data based on companies written in groupDataOnCompany.json file");
                    callbackFunction();
                }
            });
        }
    });
}

// Question 3. Get all data for company Powerpuff Brigade

function thirdQuestion(callbackFunction) {
    readFile("./data/employeesData.json", (err, data) => {
        if (err) {
            callbackFunction(err);
        } else {
            const allPowerpuffEmployees = getPowerpuffEmployees(JSON.parse(data));

            writeFile("./employees-outputs/allPowerpuffEmployees.json", allPowerpuffEmployees, (err) => {
                if (err) {
                    callbackFunction(err);
                } else {
                    console.log("All data based on Powerpuff Brigade written in allPowerpuffEmployees.json file");
                    callbackFunction();
                }
            });
        }
    });
}

// Question 4. Remove entry with id 2.

function fourthQuestion(callbackFunction) {
    readFile("./data/employeesData.json", (err, data) => {
        if (err) {
            callbackFunction(err);
        } else {
            const allDataExcludingId2 = getDataExcludingId2(JSON.parse(data));

            writeFile("./employees-outputs/allDataExcludeId2.json", allDataExcludingId2, (err) => {
                if (err) {
                    callbackFunction(err);
                } else {
                    console.log("All data except id 2, written in allDataExcludeId2.json file");
                    callbackFunction();
                }
            });
        }
    });
}

// Question 5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.

function fifthQuestion(callbackFunction) {
    readFile("./data/employeesData.json", (err, data) => {
        if (err) {
            callbackFunction(err);
        } else {
            const sortedData = getSortedData(JSON.parse(data));

            writeFile("./employees-outputs/sortDataByName.json", sortedData, (err) => {
                if (err) {
                    callbackFunction(err);
                } else {
                    console.log("All data sorted by company name and by Ids, written in allDataExcludeId2.json file");
                    callbackFunction();
                }
            });
        }
    });
}

// Question 6. Swap position of companies with id 93 and id 92.

function sixthQuestion(callbackFunction) {
    readFile("./data/employeesData.json", (err, data) => {
        if (err) {
            callbackFunction(err);
        } else {
            const swapCompany = getSwapCompanies(JSON.parse(data));

            writeFile("./employees-outputs/swapCompany.json", swapCompany, (err) => {
                if (err) {
                    callbackFunction(err);
                } else {
                    console.log("Swapped position of company with id 93 and 92, written in swapCompany.json file");
                    callbackFunction();
                }
            });
        }
    });

}

// Question 7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

function seventhQuestion(callbackFunction) {
    readFile("./data/employeesData.json", (err, data) => {
        if (err) {
            callbackFunction(err);
        } else {
            const allEmployesAddedKeyBirthdayOfEvenId = addKeyBirthdayOfIdEven(JSON.parse(data));

            writeFile("./employees-outputs/addKeyBirthday.json", allEmployesAddedKeyBirthdayOfEvenId, (err) => {
                if (err) {
                    callbackFunction(err);
                } else {
                    console.log("For every employee whose id is even, added the birthday to their information and written in addKeyBirthday.json file");
                    callbackFunction();
                }
            });
        }
    });

}

firstQuestion((err) => {
    if (err) {
        console.error(err);
    } else {
        secondQuestion((err) => {
            if (err) {
                console.error(err);
            } else {
                thirdQuestion((err) => {
                    if (err) {
                        console.error(err);
                    } else {
                        fourthQuestion((err) => {
                            if (err) {
                                console.error(err);
                            } else {
                                fifthQuestion((err) => {
                                    if (err) {
                                        console.error(err);
                                    } else {
                                        sixthQuestion((err) => {
                                            if (err) {
                                                console.error(err);
                                            } else {
                                                seventhQuestion((err) => {
                                                    if (err) {
                                                        console.error(err);
                                                    } else {
                                                        console.log("All output files created.");
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
});